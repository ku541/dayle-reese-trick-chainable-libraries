<?php

class Ball
{
    /**
     * Size of the ball.
     *
     * @var int
     */
    private $size = 1;

    /**
     * Color of the ball.
     *
     * @var string
     */
    private $color = 'blue';

    /**
     * Texture of the ball.
     *
     * @var string
     */
    private $texture = 'squishy';

    /**
     * Returns a new ball.
     *
     * @return self
     */
    public static function make()
    {
        return new self;
    }

    /**
     * Sets the size of the ball.
     *
     * @param  int $size
     * @return $this
     */
    public function size($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Sets the color of the ball.
     *
     * @param  string $color
     * @return $this
     */
    public function color($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Sets the texture of the ball.
     *
     * @param  string $texture
     * @return $this
     */
    public function texture($texture)
    {
        $this->texture = $texture;

        return $this;
    }

    /**
     * Returns a description of the ball.
     *
     * @return string
     */
    public function get()
    {
        return "This is a {$this->texture} {$this->color} ball of size {$this->size}";
    }
}

var_dump(Ball::make()->size(3)->color('red')->texture('leather')->get());